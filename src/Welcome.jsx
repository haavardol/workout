import React, {useState, useContext} from 'react'
import UserContext from './UserContext'


const Welcome = ({test}) => {
  const [counter, setCounter] = useState(0)

  const {message: {title, content}} = useContext(UserContext)

  function handleCounter() {
    setCounter(counter + 1)
  }

  return (
    <>
      <p>{`${title}: ${content}`}</p>
      <p>{counter}</p>
      <button className="button" onClick={handleCounter}>Increment</button>
    </>
  )
}

export default Welcome