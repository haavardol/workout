import React from 'react';
import './App.css';
import Welcome from './Welcome';
import {UserProvider} from './UserContext'
import {BrowserRouter as Router, Route} from 'react-router-dom'
import * as ROUTES from './constants/routes'

import LandingPage from './components/Landing'
import HomePage from './components/Home'

import Navigation from './Navigation'

function App() {
  return (
    <UserProvider value={{message: {title: 'Hello', content: 'World'}}}>
      <Router>
        <div className="App">
          <header className="App-header">
            <Navigation />
            <hr />
            <Route exact  path={ROUTES.LANDING} component={LandingPage} />
            <Route  path={ROUTES.HOME} component={HomePage} />
            <Welcome test="Hello World"/>
          </header>
        </div>
      </Router>
    </UserProvider>
  );
}

export default App;
