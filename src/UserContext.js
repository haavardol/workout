import React, {createContext} from 'react'

const UserContext = createContext({
  message: "Hello context api hooks!"
})

export const UserProvider = UserContext.Provider
export const UserConsumer = UserContext.Consumer

export default UserContext